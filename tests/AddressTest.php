<?php

namespace Concerto\Sockets\Tests;

use Concerto\Sockets\Address;
use React\EventLoop\StreamSelectLoop;

/**
 *  @covers Concerto\Sockets\Address
 */
class AddressTest extends TestCase
{
    public function testAddress()
    {
        $addr = new Address('127.0.0.1:8080');
        $this->assertEquals('tcp://127.0.0.1:8080', (string)$addr);
        $this->assertEquals('tcp', $addr->getScheme());
        $this->assertEquals('127.0.0.1', $addr->getHost());
        $this->assertEquals('8080', $addr->getPort());
        $this->assertEquals(null, $addr->getPath());
        $this->assertEquals(true, $addr->isLocal());

        $addr = new Address('[::1]');
        $this->assertEquals('tcp://[::1]', (string)$addr);
    }

    /**
     *  @expectedException  Concerto\Sockets\AddressException
     */
    public function testAddressWithPath()
    {
        $addr = new Address('[::1]:8080/path');
    }

    /**
     *  @expectedException  Concerto\Sockets\AddressException
     */
    public function testAddressWithBadScheme()
    {
        $addr = new Address('[::1]');
        $addr->setScheme(false);
    }

    /**
     *  @expectedException  Concerto\Sockets\AddressException
     */
    public function testAddressWithUnknownScheme()
    {
        $addr = new Address('[::1]');
        $addr->setScheme('donkey');
    }

    public function testUnixAddress()
    {
        $addr = new Address('unix://127.0.0.1/path');
        $this->assertEquals('unix://127.0.0.1/path', (string)$addr);
        $this->assertEquals('unix', $addr->getScheme());
        $this->assertEquals('127.0.0.1', $addr->getHost());
        $this->assertEquals(null, $addr->getPort());
        $this->assertEquals('/path', $addr->getPath());
        $this->assertEquals(true, $addr->isLocal());

        $addr = new Address('unix://[::1]/path');
        $this->assertEquals('unix://[::1]/path', (string)$addr);

        $addr = new Address('unix:///path');
        $this->assertEquals('unix:///path', (string)$addr);
    }

    /**
     *  @expectedException  Concerto\Sockets\AddressException
     */
    public function testUnixAddressWithoutPath()
    {
        $addr = new Address('unix://[::1]');
    }

    /**
     *  @expectedException  Concerto\Sockets\AddressException
     */
    public function testUnixAddressWithoutPathFalse()
    {
        $addr = new Address('unix://[::1]/path');
        $addr->setPath(false);
    }

    /**
     *  @expectedException  Concerto\Sockets\AddressException
     */
    public function testUnixAddressWithPort()
    {
        $addr = new Address('unix://[::1]:8080/path');
    }
}