<?php

namespace Concerto\Sockets\Tests;

use Concerto\Sockets\Server;
use Concerto\Sockets\Client;
use React\EventLoop\StreamSelectLoop;

/**
 *  @covers Concerto\Sockets\Server
 *  @covers Concerto\Sockets\Client
 */
class ClientTest extends TestCase
{
    protected $loop;
    protected $server;

    protected function createSession()
    {
        $loop = new StreamSelectLoop();
        $server = new Server($loop, 'tcp://127.0.0.1:1234');
        $client = new Client($loop, 'tcp://127.0.0.1:1234');

        $client->on('error', function($error)  {
            throw $error;
        });

        return [$loop, $server, $client];
    }

    public function testConnection()
    {
        list($loop, $server, $client) = $this->createSession();

        $server->on('connect', $this->expectCallableOnce());
        $server->on('connect', function() use ($server) {
            $server->shutdown();
        });
        $server->listen();

        $client->on('connect', $this->expectCallableOnce());
        $client->on('connect', function($stream) {
            $this->assertInstanceOf('React\Stream\Stream', $stream);
            $stream->end();
        });
        $client->connect();

        $loop->run();
    }

    /**
     *  @expectedException  RuntimeException
     */
    public function testInvalidConnection()
    {
        list($loop, $server, $client) = $this->createSession();

        $client->connect();

        $loop->run();
    }

    public function testReConnection()
    {
        list($loop, $server, $client) = $this->createSession();

        $server->on('connect', $this->expectCallableExactly(2));
        $server->once('connect', function($stream) use ($server) {
            $stream->close();
            $server->once('connect', function() use ($server) {
                $server->shutdown();
            });
        });
        $server->listen();

        $client->on('connect', $this->expectCallableExactly(2));
        $client->on('connect', function($stream) {
            $this->assertInstanceOf('React\Stream\Stream', $stream);
            $stream->close();
        });

        $client->once('disconnect', function() use ($loop, $client) {
            $client->connect();
            $loop->run();
        });

        $client->connect();
        $loop->run();
    }

    public function testShutdown()
    {
        list($loop, $server, $client) = $this->createSession();

        $server->on('connect', $this->expectCallableOnce());
        $server->on('connect', function() use ($server) {
            $server->shutdown();
        });
        $server->listen();

        $client->on('connect', $this->expectCallableOnce());
        $client->on('connect', function($stream) use ($client) {
            $this->assertTrue($stream->isReadable());
            $this->assertTrue($stream->isWritable());

            $client->shutdown();

            $this->assertFalse($stream->isReadable());
            $this->assertFalse($stream->isWritable());
        });
        $client->connect();

        $loop->run();
    }

    public function tearDown()
    {
        if ($this->server) {
            $this->server->shutdown();
        }
    }
}