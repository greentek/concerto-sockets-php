<?php

namespace Concerto\Sockets\Tests;

use Concerto\Sockets\Server;
use React\EventLoop\StreamSelectLoop;

/**
 *  @covers Concerto\Sockets\Address
 *  @covers Concerto\Sockets\Server
 */
class ServerTest extends TestCase
{
    protected $loop;
    protected $server;

    public function setUp()
    {
        $this->loop = new StreamSelectLoop();

        $this->server = new Server($this->loop, 'tcp://127.0.0.1:1234');
        $this->server->listen();

        $this->address = $this->server->getAddress();
    }

    /**
     *  @expectedException BadMethodCallException
     */
    public function testDisabledGetPort()
    {
        $this->server->getPort();
    }

    /**
     *  @expectedException InvalidArgumentException
     */
    public function testDisabledListen()
    {
        $this->server->listen(8080);
    }

    /**
     *  @expectedException React\Socket\ConnectionException
     */
    public function testCouldNotBind()
    {
        $server = new Server($this->loop, 'tcp://127.0.0.1:1234');
        $server->listen();
    }

    public function testConnection()
    {
        $client = stream_socket_client($this->address);

        $this->server->on('connect', $this->expectCallableOnce());

        $this->loop->tick();
    }

    public function testManyConnections()
    {
        $client1 = stream_socket_client($this->address);
        $client2 = stream_socket_client($this->address);
        $client3 = stream_socket_client($this->address);

        $this->server->on('connect', $this->expectCallableExactly(3));

        $this->loop->tick();
        $this->loop->tick();
        $this->loop->tick();
    }

    public function testData()
    {
        $data = md5(rand());
        $client = stream_socket_client($this->address);
        $mock = $this->expectCallableWithData($data);

        $this->server->on('connect', function($conn) use ($mock) {
            $conn->on('data', $mock);
        });

        fwrite($client, $data);

        $this->loop->tick();
        $this->loop->tick();
    }

    public function testDataWithoutData()
    {
        $data = md5(rand());
        $client = stream_socket_client($this->address);
        $mock = $this->expectCallableNever();

        $this->server->on('connect', function($conn) use ($mock) {
            $conn->on('data', $mock);
        });

        $this->loop->tick();
        $this->loop->tick();
    }

    public function testDisconnect()
    {
        $client = stream_socket_client($this->address);
        $mock = $this->expectCallableOnce();

        $this->server->on('connect', function($conn) use ($mock) {
            $conn->on('end', $mock);
        });

        fclose($client);

        $this->loop->tick();
        $this->loop->tick();
    }

    public function testDisconnectWithoutDisconnect()
    {
        $client = stream_socket_client($this->address);
        $mock = $this->expectCallableNever();

        $this->server->on('connect', function($conn) use ($mock) {
            $conn->on('end', $mock);
        });

        $this->loop->tick();
        $this->loop->tick();
    }

    public function tearDown()
    {
        if ($this->server) {
            $this->server->shutdown();
        }
    }
}