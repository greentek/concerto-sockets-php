<?php

namespace Concerto\Sockets;

interface AddressInterface
{
    public function __toString();

    public function isLocal();

    public function getAddress();
    public function getHost();
    public function getPath();
    public function getPort();
    public function getScheme();

    public function setAddress($address);
    public function setHost($host);
    public function setPath($path);
    public function setPort($port);
    public function setScheme($scheme);
}