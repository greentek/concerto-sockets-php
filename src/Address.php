<?php

namespace Concerto\Sockets;

class Address implements AddressInterface
{
    protected $host;
    protected $local;
    protected $path;
    protected $port;
    protected $scheme;

    public function __construct($address)
    {
        $this->setAddress($address);
    }

    public function __toString()
    {
        return $this->getAddress();
    }

    public function isLocal()
    {
        return $this->local;
    }

    public function getAddress()
    {
        $address = $this->scheme . '://';

        // Enclose IPv6 addresses in square brackets before appending port:
        if (strpos($this->host, ':') !== false) {
            $this->host = '[' . $this->host . ']';
        }

        $address .= $this->host;

        if ($this->port) {
            $address .= ':' . $this->port;
        }

        $address .= $this->path;

        return $address;
    }

    public function getHost()
    {
        return $this->host;
    }

    public function getPath()
    {
        return $this->path;
    }

    public function getPort()
    {
        return $this->port;
    }

    public function getScheme()
    {
        return $this->scheme;
    }

    public function setAddress($address)
    {
        $scheme = false;

        // Assume a TCP scheme if none given:
        if (false === strpos($address, '://')) {
            $address = 'tcp://' . $address;
        }

        // Allow unix domain sockets to pass parse_url:
        else if (0 === strpos($address, 'unix://') || 0 === strpos($address, 'udg://')) {
            $scheme = strtok($address, ':');
            $address = str_replace('unix://', 'file://', $address);
        }

        $this->setScheme(
            $scheme ?: substr($address, 0, strpos($address, '://'))
        );
        $this->setHost(parse_url($address, PHP_URL_HOST));
        $this->setPort(parse_url($address, PHP_URL_PORT));
        $this->setPath(parse_url($address, PHP_URL_PATH));
    }

    public function setHost($host)
    {
        $this->host = (trim($host, '[]') ?: null);
        $this->local = (
            null === $this->host
            || '::1' === $this->host
            || 0 === strpos($this->host, '127.')
        );

        if (false === in_array($this->scheme, ['unix', 'udg']) && false === isset($this->host)) {
            throw new AddressException(
                "Expected host for scheme '{$this->scheme}'.",
                AddressException::EXPECTED_HOST
            );
        }
    }

    public function setPath($path)
    {
        if (false === $path) {
            $path = null;
        }

        $this->path = $path;

        if (in_array($this->scheme, ['unix', 'udg']) && false === isset($this->path)) {
            throw new AddressException(
                "Expected path for scheme '{$this->scheme}'.",
                AddressException::EXPECTED_PATH
            );
        }

        else if (false === in_array($this->scheme, ['unix', 'udg']) && isset($this->path)) {
            throw new AddressException(
                "Unexpected path for scheme '{$this->scheme}'.",
                AddressException::UNEXPECTED_PATH
            );
        }
    }

    public function setPort($port)
    {
        $this->port = $port;

        if (in_array($this->scheme, ['unix', 'udg']) && isset($this->port)) {
            throw new AddressException(
                "Unexpected port for scheme '{$this->scheme}'.",
                AddressException::UNEXPECTED_PORT
            );
        }
    }

    public function setScheme($scheme)
    {
        $this->scheme = $scheme;

        if (false === isset($this->scheme) || !$this->scheme) {
            throw new AddressException(
                'Missing transport scheme.',
                AddressException::MISSING_SCHEME
            );
        }

        else if (false === in_array($this->scheme, stream_get_transports())) {
            throw new AddressException(
                "Unsupported transport scheme '{$this->scheme}'.",
                AddressException::UNSUPPORTED_SCHEME
            );
        }
    }
}