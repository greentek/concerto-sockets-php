<?php

namespace Concerto\Sockets;

use Exception;

class AddressException extends Exception
{
    const MISSING_SCHEME = 1;
    const UNSUPPORTED_SCHEME = 2;
    const EXPECTED_HOST = 3;
    const EXPECTED_PATH = 4;
    const UNEXPECTED_PATH = 5;
    const UNEXPECTED_PORT = 6;
}