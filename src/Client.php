<?php

namespace Concerto\Sockets;

use Evenement\EventEmitterTrait;
use RuntimeException;
use React\EventLoop\LoopInterface;
use React\Socket\Connection;
use React\SocketClient\ConnectionException;
use React\Stream\Stream;

/**
 *  @event connection
 *  @event error
 */
class Client
{
    use EventEmitterTrait;

    protected $address;
    protected $conn;
    protected $loop;
    protected $socket;

    public function __construct(LoopInterface $loop, $address)
    {
        if (false === ($address instanceof AddressInterface)) {
            $address = new Address($address);
        }

        $this->address = $address;
        $this->loop = $loop;
    }

    public function connect()
    {
        $this->socket = stream_socket_client($this->address, $errno, $errstr, 0, STREAM_CLIENT_CONNECT | STREAM_CLIENT_ASYNC_CONNECT);

        if (!$this->socket) {
            throw new ConnectionException(
                "Connection to {$this->address} failed: {$errstr}", $errno
            );
        }

        stream_set_blocking($this->socket, 0);

        $this->loop->addWriteStream($this->socket, function() {
            $this->loop->removeWriteStream($this->socket);

            // The following hack looks like the only way to
            // detect connection refused errors with PHP's stream sockets.
            if (false === stream_socket_get_name($this->socket, true)) {
                $this->emit('error', [
                    new RuntimeException(
                        "Connection to {$this->address} refused"
                    )
                ]);
                return;
            }

            $this->handleConnection();
        });
    }

    public function getAddress()
    {
        return $this->address;
    }

    protected function handleConnection()
    {
        $this->conn = new Connection($this->socket, $this->loop);

        $this->conn->on('end', function() {
            $this->handleDisconnection();
        });

        $this->emit('connect', [$this->conn]);
    }

    protected function handleDisconnection()
    {
        if (is_resource($this->socket)) {
            $socket = $this->socket;
            $this->socket = null;
            $this->conn = null;

            $this->loop->removeStream($socket);
            fclose($socket);

            $this->emit('disconnect');
        }
    }

    public function shutdown()
    {
        if (isset($this->conn)) {
            $this->conn->close();
        }

        $this->removeAllListeners();
    }
}