<?php

namespace Concerto\Sockets;

use BadMethodCallException;
use InvalidArgumentException;
use RuntimeException;
use React\EventLoop\LoopInterface;
use React\Socket\ConnectionException;
use React\Socket\Server as BaseSocketServer;

/**
 *  @event connection
 *  @event error
 */
class Server extends BaseSocketServer
{
    protected $address;
    protected $loop;

    public function __construct(LoopInterface $loop, $address)
    {
        if (false === ($address instanceof AddressInterface)) {
            $address = new Address($address);
        }

        $this->address = $address;
        $this->loop = $loop;

        parent::__construct($loop);
    }

    public function listen($port = null, $host = null)
    {
        if (isset($port) || isset($host)) {
            throw new InvalidArgumentException('Server was created using Concerto\Sockets\Factory, no arguments are allowed');
        }

        $this->master = @stream_socket_server($this->address, $errno, $errstr);

        if (false === $this->master) {
            throw new ConnectionException("Could not bind to {$this->address}: {$errstr}", $errno);
        }

        stream_set_blocking($this->master, 0);

        $this->loop->addReadStream($this->master, [$this, 'handleConnection']);
    }

    public function getAddress()
    {
        return $this->address;
    }

    public function getPort()
    {
        throw new BadMethodCallException('Server was created using Concerto\Sockets\Factory, the method getPort is not available');
    }

    public function handleConnection($master)
    {
        $socket = stream_socket_accept($master);

        if (false === $socket) {
            $this->emit('error', [
                new RuntimeException('Error accepting new connection')
            ]);
            return;
        }

        stream_set_blocking($socket, 0);

        $client = $this->createConnection($socket);
        $this->emit('connection', array($client));
        $this->emit('connect', array($client));
    }
}