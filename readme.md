# Sockets Component

Library for creating event based socket servers and clients based on ReactPHP.

[![Build Status](https://secure.travis-ci.org/concertophp/sockets.png?branch=master)](http://travis-ci.org/concertophp/sockets)


## Install

The recommended way to install Sockets is [through composer](http://getcomposer.org).

```JSON
{
    "require": {
        "concerto/sockets": "0.*"
    }
}
```


## Usage
### Server

```php
use Concerto\Sockets\Server;
use React\EventLoop\Factory as EventLoopFactory;

$loop = EventLoopFactory::create();
$server = new Server($loop, 'tcp://127.0.0.1:1234');

$server->on('connect', function($conn) {
	$conn->write("Hello client!\n");

	$conn->on('data', function ($data) use ($conn) {
		$conn->close();
	});
});

$server->listen();
$loop->run();
```

### Client

```php
use Concerto\Sockets\Client;
use React\EventLoop\Factory as EventLoopFactory;
use React\Stream\Stream;

$loop = EventLoopFactory::create();
$client = new Client($loop, 'tcp://127.0.0.1:1234');

$client->on('connect', function($conn) use ($loop) {
	$conn->pipe(new Stream(STDOUT, $loop));
	$conn->write("Hello World!\n");
});

$client->connect();
$loop->run();
```